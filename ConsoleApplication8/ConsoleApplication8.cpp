#include <iostream>

template <class T> class Stack
{
public:
    void Push(T item)
    {
        T* newArr = new T[size + 1];
        for (unsigned int i = 0; i < size; ++i)
        {
            newArr[i] = arr[i];
        }
        delete arr;

        arr = newArr;
        arr[size] = item;
        size += 1;
    }

    T Pop()
    {
        return arr[size - 1];
    }

private:
    T* arr;
    unsigned int size = 0;
};

int main()
{
    Stack<std::string>* st = new Stack<std::string>();

    st->Push("text");
    std::cout << st->Pop();
    return 0;
}